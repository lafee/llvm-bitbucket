set -x
#module load bitbucket

clang -S -emit-llvm $1.c -o $1.s

opt -load ../../build/Release+Asserts/lib/LLVMSPIRE.so -SPIRERMAtoLS $1.s -o $1.opt

#module load polly

#opt -loop-vectorize -force-vector-width=8 /< $1.s />$1.opt.vect

#ca a marche
#clang -Rpass=loop-vectorize -S -emit-llvm transpose1.c -o transpose1.vect 

#opt -basiccg -simplifycfg -loop-simplify  -inline-cost

#generate memcpy
opt  -basicaa  -loop-idiom $1.opt -o $1.opt.vect

llvm-dis $1.opt.vect

echo -e "\n \n \n Hello, this is the result \n \n \n"
echo $1

more $1.opt.vect.ll

