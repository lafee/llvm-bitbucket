; ModuleID = 'test-bounds.opt'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@main.targ_static = internal global [11 x i32] zeroinitializer, align 16
@targg = common global [11 x i32] zeroinitializer, align 16
@srcg = common global [11 x i32] zeroinitializer, align 16
@lsrcg = common global [11 x i64] zeroinitializer, align 16

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %src = alloca [11 x i32], align 16
  %ltarg = alloca [11 x i64], align 16
  %nbr = alloca i32, align 4
  %my_pe = alloca i32, align 4
  %npes = alloca i32, align 4
  store i32 0, i32* %retval
  call void @start_pes(i32 0)
  store i32 2, i32* %nbr, align 4
  %0 = load i32* %nbr, align 4
  %RMA = load i8* bitcast (i32* getelementptr (i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i32 0), i64 11) to i8*), !RMA !{i32 %0}
  store i8 %RMA, i8 addrspace(100)* addrspacecast (i8* bitcast (i32* getelementptr (i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i32 0), i64 11) to i8*) to i8 addrspace(100)*)
  %arraydecay = getelementptr inbounds [11 x i32]* %src, i32 0, i32 0
  %1 = load i32* %nbr, align 4
  %arraysectionsrc = getelementptr i32* %arraydecay, i64 10
  %remote.cast = addrspacecast i32* %arraysectionsrc to i32* addrspace(100)*
  %RMA1 = load i8* bitcast (i32* getelementptr inbounds ([11 x i32]* @main.targ_static, i32 0, i64 10) to i8*), !RMA !{i32 %1}
  %cstr = bitcast i32* addrspace(100)* %remote.cast to i8 addrspace(100)*
  store i8 %RMA1, i8 addrspace(100)* %cstr
  %arraydecay1 = getelementptr inbounds [11 x i64]* %ltarg, i32 0, i32 0
  %arraysectiontar = getelementptr i64* %arraydecay1, i64 21
  %remote.cast2 = addrspacecast i64* %arraysectiontar to i64* addrspace(100)*
  %cstr3 = bitcast i64* addrspace(100)* %remote.cast2 to i8 addrspace(100)*
  %RMA4 = load i8 addrspace(100)* %cstr3, !RMA !1
  store i8 %RMA4, i8* bitcast (i64* getelementptr (i64* getelementptr inbounds ([11 x i64]* @lsrcg, i32 0, i32 0), i64 21) to i8*)
  ret i32 1
}

declare void @start_pes(i32) #1

declare void @shmem_int_put(i32*, i32*, i64, i32) #1

declare void @shmem_long_get(i64*, i64*, i64, i32) #1

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
!1 = metadata !{i32 4}
