#include<stdio.h>
#include<stdlib.h>
#include "shmem.h"
#include <complex.h> 
//#include <mpi.h>
#define n 2
#define m 2
#define np 2

//  double complex x1(ntdivnp), x2(ntdivnp)
double  xin[m][n/np][np], xout[m*np][n/np];




  

int main (argc, argv)
     int argc;
     char *argv[];
{


  //int rank, size;
 
  //MPI_Init(&argc, &argv);	/* starts MPI */
  //MPI_Comm_rank (MPI_COMM_WORLD, &rank);	/* get current process id */
  //MPI_Comm_size (MPI_COMM_WORLD, &size);	/* get number of processes */
  // printf( "Hello world from process %d of %d\n", rank, size );



  //      include 'global.h'
  int ioff;
  int i, j, p;
  int ntdivnp = n*m;
  //double complex xin(n2, n1/np2, 0:np2-1), xout(n2*np2, n1/np2);
  int k =0;
  for( p = 0; p<m;p++){
    ioff = p*m;
    for(j = 0; j<n/np;j++){
      for(i = 0;i< np;i++){
	//tmp = xin[i][j][p];
        xin[p][j][i] = k++;
      }
    }
  }
  


  //transpose global
  // MPI_Alltoall(xout, ntdivnp/np, MPI_DOUBLE_COMPLEX,
  //	       xin, ntdivnp/np, MPI_DOUBLE_COMPLEX,
  //	       MPI_COMM_WORLD);
  int nb = ntdivnp/np; int me;
  for (p=0; p<np; p++)
    //do 10, j1=0,np2-1
    shmem_double_put(xout[nb*me+1],xin[nb*p+1], nb,p);

     

  //optimization
  /*for( p = 0; p<np2;p++){
    ioff = p*n2;
    for(j = 1; j<=n1/np2;j++){
      for(i = 1;i<= n2;i++)
	xout[i+ioff, j] = xin[i, j, p];
    }
    }*/
  // double complex xin[m][n/np][np], xout[m*np][n/np];

  //transpose finish
  printf("finish mpialltoall\n");
  for( p = 0; p<np;p++){
    ioff = p*m;
    for(j = 0; j<n/np;j++){
      for(i = 0;i< m;i++){
	xout[i+ioff][j] = xin[i][j][p];
      }
    }
  }
  printf("finish transposel\n");
  
  int it_no=0;
    for(j = 0; j< n/np;j++){
      for(i = 0;i< m*np;i++){
	//printf(' %17.0f\t %17.7f %11.7f*i\t %17.7f %11.7f*i\t\n',[it_no,real(xout[i][j]),imag(xout[i][j]),real(g(xout[i][j])),imag(g(xout[i][j]))])
	//printf("%g + i%g\n", xout[i][j]);
	printf("%f\n", xout[i][j]);
      }
    }
    //MPI_Finalize();
  
  return 0;

}
