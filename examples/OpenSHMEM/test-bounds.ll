; ModuleID = 'test-bounds.bc'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@main.targ_static = internal global [8 x i32] zeroinitializer, align 16
@targg = common global [8 x i32] zeroinitializer, align 16
@srcg = common global [8 x i32] zeroinitializer, align 16
@lsrcg = common global [8 x i64] zeroinitializer, align 16

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %src = alloca [8 x i32], align 16
  %ltarg = alloca [8 x i64], align 16
  %nbr = alloca i32, align 4
  %my_pe = alloca i32, align 4
  %npes = alloca i32, align 4
  store i32 0, i32* %retval
  call void @start_pes(i32 0)
  store i32 2, i32* %nbr, align 4
  %0 = load i32* %nbr, align 4
  call void @shmem_int_put(i32* getelementptr inbounds ([8 x i32]* @targg, i32 0, i32 0), i32* getelementptr inbounds ([8 x i32]* @srcg, i32 0, i32 0), i64 8, i32 %0)
  %arraydecay = getelementptr inbounds [8 x i32]* %src, i32 0, i32 0
  %1 = load i32* %nbr, align 4
  call void @shmem_int_put(i32* getelementptr inbounds ([8 x i32]* @main.targ_static, i32 0, i32 0), i32* %arraydecay, i64 8, i32 %1)
  %arraydecay1 = getelementptr inbounds [8 x i64]* %ltarg, i32 0, i32 0
  call void @shmem_long_get(i64* %arraydecay1, i64* getelementptr inbounds ([8 x i64]* @lsrcg, i32 0, i32 0), i64 18, i32 4)
  ret i32 1
}

declare void @start_pes(i32) #1

declare void @shmem_int_put(i32*, i32*, i64, i32) #1

declare void @shmem_long_get(i64*, i64*, i64, i32) #1

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
