#define N 11
#define M 10
#include "shmem.h"
int targg[N];
long lsrcg[N];
int srcg[N]; 

int main(void) {
  int i, src[N];
  long ltarg[N]; 
  static int targ_static[N];
  int nbr;  
  int my_pe, npes;

  start_pes(0);
  
  nbr = 2;
  
  shmem_int_put(targg, srcg, N, nbr);    
  shmem_int_put(targ_static, src, M, nbr);
  shmem_long_get(ltarg,lsrcg,N+M,4);
  
  //shmem_barrier_all();  /* sync sender and receiver */
  return 1;                
}


