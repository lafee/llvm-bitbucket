#include <stdio.h>

//#define N 11
#include "shmem.h"
//int div = 7;

int foo(int i, int div){
  return (i+div)/2;
}

int main(int div) {
  int i=0, comm_size = 8, me, pe;
  int nbr, N;  
  int my_pe, npes;
  int a=12,b=42, size=4;
  int targg[N];
  int src[N], other[N];
  //start_pes(0);
  
  nbr = 0;
  
  targg[0] = i;
  targg[4] = i;
  targg[5] = i;
  src[0] = a;
  src[4] = b;
  src[5] = b;
  
  for (i=0;i<N;i++)
    other[i] = foo(i, div);
  
  for(me=0;me<comm_size;me++){
    for(pe=0;pe<comm_size;pe++){
      shmem_int_put(&targg[other[me]],&src[other[pe]],1,other[pe]);
      other[pe] = targg[other[me]] + 1;
    //targg[0] = src[i];
    }
  }
    
  printf("targg div= %d, %d, %d, %d \n", targg[0],targg[4],targg[5], targg[other[div]]);
  printf("src = %d, %d,%d \n", src[0],src[4], src[5]);	
  return 1;                
}


