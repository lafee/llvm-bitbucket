#include <stdio.h>

#define N 11
#define M 10
#include "shmem.h"
int targg[N];
int srcg[N]; 

int main(void) {
  int i=0;
  int nbr;  
  int my_pe, npes;
  int a=12,b=42, size=4;
  //start_pes(0);
  
  nbr = 0;
  
  targg[0] = i;
  targg[4] = i;
  targg[5] = i;
  srcg[0] = a;
  srcg[4] = b;
  srcg[5] = b;
  shmem_int_get(targg, srcg, size, nbr);    
  
  printf("targg = %d, %d, %d \n", targg[0],targg[4],targg[5]);
  printf("src = %d, %d,%d \n", srcg[0],srcg[4], srcg[5]);	
  return 1;                
}


