set -x
#module load bitbucket

clang -S -emit-llvm $1.c -o $1.s

#opt -load ../../build/Release+Asserts/lib/LLVMSPIRE.so -SPIRERMAtoLS < $1.s > $1.opt

#module load polly

opt -load /home/dounia/src/polly-llvm-3.5.0/tools/polly/build/lib/LLVMPolly.so -S -polly-canonicalize $1.s -o $1.opt.tile.ll

#opt -load /home/dounia/src/polly-llvm-3.5.0/tools/polly/build/lib/LLVMPolly.so $1.opt.tile -polly-opt-isl -basicaa  -polly-cloog -polly-codegen -polly-no-tiling=0 -polly-ast -polly-tile-sizes=64 -o $1.opt.tile.bc

#opt -load /home/dounia/src/polly-llvm-3.5.0/tools/polly/build/lib/LLVMPolly.so $1.s -polly-opt-isl -basicaa  -polly-cloog -polly-codegen -polly-no-tiling=0 -polly-ast -polly-tile-sizes=16  -print-after-all -o $1.opt.tile.bc

#opt  -load /home/dounia/src/polly-llvm-3.5.0/tools/polly/build/lib/LLVMPolly.so    -polly-opt-isl -basicaa  -polly-codegen -polly-no-tiling=0 -polly-ast -polly-tile-sizes=16  $1.opt.tile.ll -o $1.opt.tile1.bc

opt -load /home/dounia/src/polly-llvm-3.5.0/tools/polly/build/lib/LLVMPolly.so $1.opt.tile.ll -polly-opt-isl -basicaa -polly -polly-codegen -polly-no-tiling=0 -polly-tile-sizes=1024 -polly-report -polly-show -polly-export-jscop -o $1.opt.tile1.bc
#|FileCheck $1.opt.tile.ll --check-prefix=TILING

#opt -load /home/dounia/src/polly-llvm-3.5.0/tools/polly/build/lib/LLVMPolly.so $1.opt.tile.ll  -O2 -o $1.opt.tile1.bc

llvm-dis $1.opt.tile1.bc 

echo -e "\n \n \n oct 22, Hello, this is the result \n \n \n"
echo $1

more $1.opt.tile1.ll

