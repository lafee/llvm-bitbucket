; ModuleID = 'test-put.opt'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@targg = common global [11 x i32] zeroinitializer, align 16
@srcg = common global [11 x i32] zeroinitializer, align 16
@.str = private unnamed_addr constant [21 x i8] c"targg = %d, %d, %d \0A\00", align 1
@.str1 = private unnamed_addr constant [18 x i8] c"src = %d, %d,%d \0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %i = alloca i32, align 4
  %nbr = alloca i32, align 4
  %my_pe = alloca i32, align 4
  %npes = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %size = alloca i32, align 4
  store i32 0, i32* %retval
  store i32 0, i32* %i, align 4
  store i32 12, i32* %a, align 4
  store i32 42, i32* %b, align 4
  store i32 4, i32* %size, align 4
  store i32 0, i32* %nbr, align 4
  %0 = load i32* %i, align 4
  store i32 %0, i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i64 0), align 4
  %1 = load i32* %i, align 4
  store i32 %1, i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i64 4), align 4
  %2 = load i32* %i, align 4
  store i32 %2, i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i64 5), align 4
  %3 = load i32* %a, align 4
  store i32 %3, i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i64 0), align 4
  %4 = load i32* %b, align 4
  store i32 %4, i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i64 4), align 4
  %5 = load i32* %b, align 4
  store i32 %5, i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i64 5), align 4
  %6 = load i32* %size, align 4
  %conv = sext i32 %6 to i64
  %7 = load i32* %nbr, align 4
  %arraysectionsrc = getelementptr i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i32 0), i64 %conv
  %arraysectiontar = getelementptr i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i32 0), i64 %conv
  %RMA = load i32* %arraysectionsrc
  store i32 %RMA, i32* %arraysectiontar
  %8 = load i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i64 0), align 4
  %9 = load i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i64 4), align 4
  %10 = load i32* getelementptr inbounds ([11 x i32]* @targg, i32 0, i64 5), align 4
  %call = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([21 x i8]* @.str, i32 0, i32 0), i32 %8, i32 %9, i32 %10)
  %11 = load i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i64 0), align 4
  %12 = load i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i64 4), align 4
  %13 = load i32* getelementptr inbounds ([11 x i32]* @srcg, i32 0, i64 5), align 4
  %call1 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([18 x i8]* @.str1, i32 0, i32 0), i32 %11, i32 %12, i32 %13)
  ret i32 1
}

declare void @shmem_int_put(i32*, i32*, i64, i32) #1

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
