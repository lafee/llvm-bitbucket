#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/MemoryDependenceAnalysis.h"
#include "llvm/IR/Metadata.h"

#include "llvm/IR/InstIterator.h" // provides the class "inst_iterator" to iterate through each instruction in a function
#include "llvm/IR/Instructions.h"
#include<vector>
#include "llvm/IR/Type.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
//#include "Bitcode/Writer/ValueEnumerator.h"



#define SHM_PUT_FUNC_CNT 29
#define SHM_RMA_FUNC_CNT 12

using namespace llvm;


namespace {
  struct SPIRERMAtoLS : public FunctionPass {
    LLVMContext* Context;
    static char ID;

    SPIRERMAtoLS() : FunctionPass(ID) {}

    std::string shmemFuncList[SHM_PUT_FUNC_CNT] = {
		"shmem_char_put",
		"shmem_short_put",
		"shmem_int_put",
		"shmem_long_put",
		"shmem_float_put",
		"shmem_double_put",
		"shmem_longlong_put",
		"shmem_longdouble_put",
		"shmem_put32",
		"shmem_put64",
		"shmem_put128",
		"shmem_putmem",
		"shmem_char_get",
		"shmem_short_get",
		"shmem_int_get",
		"shmem_long_get",
		"shmem_float_get",
		"shmem_double_get",
		"shmem_longlong_get",
		"shmem_longdouble_get",
		"shmem_get32",
		"shmem_get64",
		"shmem_get128",
		"shmem_getmem",
		"start_pes",
		"my_pe",
		"num_pes",
		"shmem_barrier_all",
		"shmem_barrier"
    };

    std::string shmemRMAPutList[SHM_RMA_FUNC_CNT] = {
		"shmem_char_put",
		"shmem_short_put",
		"shmem_int_put",
		"shmem_long_put",
		"shmem_float_put",
		"shmem_double_put",
		"shmem_longlong_put",
		"shmem_longdouble_put",
		"shmem_put32",
		"shmem_put64",
		"shmem_put128",
		"shmem_putmem",
    };
    std::string shmemRMAGetList[SHM_RMA_FUNC_CNT] = {
		"shmem_char_get",
		"shmem_short_get",
		"shmem_int_get",
		"shmem_long_get",
		"shmem_float_get",
		"shmem_double_get",
		"shmem_longlong_get",
		"shmem_longdouble_get",
		"shmem_get32",
		"shmem_get64",
		"shmem_get128",
		"shmem_getmem",
    };

    bool isShmemFunc(std::string searchString) {
	bool found=false;
	for (int i=0;i<SHM_PUT_FUNC_CNT;i++) { 
	    if(searchString.compare(shmemFuncList[i])==0) {
		found = true;	
		break;
	    }
	}
	return found;
    }
    bool isRMAPutFunc(std::string searchString) {
	bool found=false;
	for (int i=0;i<SHM_RMA_FUNC_CNT;i++) { 
	    if(searchString.compare(shmemRMAPutList[i])==0) {
		found = true;	
		break;
	    }
	}
	return found;
    }

    bool isRMAGetFunc(std::string searchString) {
	bool found=false;
	for (int i=0;i<SHM_RMA_FUNC_CNT;i++) { 
	    if(searchString.compare(shmemRMAGetList[i])==0) {
		found = true;	
		break;
	    }
	}
	return found;
    }

    /*Convert shmem_put(target, source) to convert_addrspace(src) to 100; RMA  = load(target); store(RMA, src)
      Convert shmem_get(target, source) to convert_addrspace(target) to 100; RMA  = load(target); store(RMA, src)*/
    Instruction *generateLoadStore(CallInst *callInst, Value *Variable, LLVMContext& C, IRBuilder<> &Builder) {
      //IRBuilder<> Builder(callInst);
      Instruction* storenew, *loadnew;
      Value *remoteV, *Char2, *Char;
      Function *targetFunc = callInst->getCalledFunction();
      errs() << "RMA ah bon Simplify RMA Found 111"  <<"\n";
      if (targetFunc) {
	llvm::Value *args[2] = {/*Builder.getInt64(0)*/Variable, Variable/*callInst->getArgOperand(2)*/};
	llvm::ArrayRef<llvm::Value *> T(&args[0], &args[1]);
	Value *GEPsrc = Builder.CreateGEP(callInst->getArgOperand(1), T, "addressSrc");
	Value *GEPtar = Builder.CreateGEP(callInst->getArgOperand(0), T, "addressTar");
	StringRef targetFuncName = targetFunc->getName();
	if (isRMAPutFunc(std::string(targetFuncName))){
	  errs() << "RMA ah bon Simplify RMA Found 1"  <<"\n";
	  //remoteV = CastToRemote(callInst->getArgOperand(0), Builder);
	  /// Construct an ArrayRef from a single element end if index. 
	  
	  //Char = Builder.CreateLoad(CastToCStr(callInst->getArgOperand(1), Builder),"RMA"); 
	  Char = Builder.CreateLoad(GEPsrc,"RMA"); 
	  loadnew =  dyn_cast<Instruction>(Char);
	  Char2 = Builder.CreateStore(Char, GEPtar, false);//true/*CastToCStr(GEPtarremoteV, Builder)*/);//true for volatile
	  storenew =  dyn_cast<Instruction>(Char2);
	  storenew->setMetadata("PE", MDNode::get(C, callInst->getOperand(3)));
	}
	if (isRMAGetFunc(std::string(targetFuncName))){
	  //remoteV = CastToRemote(callInst->getArgOperand(1), Builder);
	  Char = Builder.CreateLoad(GEPsrc/*CastToCStr(remoteV, Builder)*/,"RMA");
	  loadnew =  dyn_cast<Instruction>(Char);
	  loadnew->setMetadata("PE", MDNode::get(C, callInst->getOperand(3)));
	  Char2 = Builder.CreateStore(Char, GEPtar/*CastToCStr(callInst->getArgOperand(0), Builder)*/);
	  storenew =  dyn_cast<Instruction>(Char2);
	}
	errs() << "RMA ah bon Simplify RMA Found  inew"  <<"\n";	  
	} 
      return storenew;
   }



    BasicBlock *createLoop(IRBuilder<> &Builder, BasicBlock *currentb, BasicBlock *newb, CallInst *callInst, LLVMContext& C){
      // Make the new basic block for the loop header, inserting after current
      // block.
      Function *TheFunction = currentb->getParent();
      BasicBlock *LoopBB = BasicBlock::Create(getGlobalContext(), "LoadStoreLoop", TheFunction);  
      std::string VarName = "lafee";
      // Start insertion in LoopBB.
      Builder.SetInsertPoint(LoopBB);
      errs() << "loop ah bon Simplify RMA Found 2"  <<"\n";
      // Start the PHI node with an entry for Start.
      PHINode *Variable = Builder.CreatePHI(Type::getInt64Ty(getGlobalContext()), 2, VarName.c_str());
      Value *startVal =  ConstantInt::get(getGlobalContext(), APInt(64,0,false));
      Variable->addIncoming(startVal, currentb);
      // Insert an explicit fall through from the current block to the LoopBB.
      TerminatorInst *T = currentb->getTerminator();
      BranchInst *BI = dyn_cast<BranchInst>(T);
      BI->setSuccessor(0, LoopBB);

      generateLoadStore(callInst, Variable, C, Builder);
      // Emit the step value.
      Value *StepVal = ConstantInt::get(getGlobalContext(), APInt(64, 1, false));
      Value *NextVar = Builder.CreateAdd(Variable, StepVal, "nextvar");

      Value *size = callInst->getOperand(2);
      Value *EndCond = NextVar;//Variable;
      EndCond = Builder.CreateICmpULT(EndCond,  size, "cmptmp");

      // Convert condition to a bool by comparing equal to 0.0.
      ////Value *EndCond = NextVar;//size is int callInst->getOperand(2);
      //EndCond = Builder.CreateIntCast(EndCond, Builder.getInt64Ty(), false);// "booltmp");
      //EndCond = Builder.CreateICmpEQ(EndCond, ConstantInt::get(getGlobalContext(), APInt(64, 0, false)), "loopcond");
      // Create the "after loop" block and insert it.
      BasicBlock *LoopEndBB = Builder.GetInsertBlock();
      BasicBlock *AfterBB = newb;
      // Insert the conditional branch into the end of LoopEndBB.
      Builder.CreateCondBr(EndCond, LoopBB, AfterBB);
      // Any new code will be inserted in AfterBB.
      Builder.SetInsertPoint(AfterBB);
      // Add a new entry to the PHI node for the backedge.
      Variable->addIncoming(NextVar, LoopEndBB);
      errs() << "ah bon Simplify RMA Found  6"  <<"\n";
      return LoopBB;//newb;
    }


    // Convert address space from 0 (generic) to remote (100)
    //Do we need this information because I have already this in the metadat
    Value *CastToRemote(Value *V, IRBuilder<> &B) {
      //unsigned globalSpace = 100;
      ////Type *IntNPtr = Type::getIntNPtrTy(V->getContext(),100);
      //Type* int8PtrTy =  B.getInt8PtrTy(AS);
      //Type* globalInt8PtrTy =  (V->getType())->getPointerTo(globalSpace);
      return V;//  B.CreatePointerCast(V, /*globalInt8PtrTy*/p, "remote");
      ////V->mutateType(globalInt8PtrTy); 
    }

    //CastToCStr - Return V if it is an i8*, otherwise cast it to i8*.
    Value *CastToCStr(Value *V, IRBuilder<> &B) {
      unsigned AS = V->getType()->getPointerAddressSpace();
      return B.CreateBitCast(V,  B.getInt8PtrTy(AS), "cstr");
    }

    /*convert put/get shmem functions to a loop of load/store operations*/
    void simplify_RMA(CallInst *callInst, LLVMContext& C, BasicBlock *currentb, BasicBlock *newb) {
      IRBuilder<> Builder(callInst);
      BasicBlock *AfterLoop;
      AfterLoop = createLoop(Builder, currentb, newb, callInst, C);
      errs() << "ah bon Simplify RMA Found  7"  <<"\n";
      return;
   }

    virtual bool runOnFunction(Function &F) {
      //errs() << "Simplify RMA Found "<<"\n";
      BasicBlock::iterator i, ie;
      bool isSplit;
      for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b) {	
	for (isSplit = false, /*BasicBlock::iterator*/ i = b->begin(), ie = b->end(); (isSplit != true) && (i != ie); ++i) {
	  if (CallInst* callInst = dyn_cast<CallInst>(&*i)) {
	    // We know we've encountered a call instruction, so we
	    // need to determine if it's a shmem-function or not
	    Function *targetFunc = callInst->getCalledFunction();
	    LLVMContext& C = i->getContext();
	    if (targetFunc) {
	      StringRef targetFuncName = targetFunc->getName();
	      if (isRMAPutFunc(std::string(targetFuncName)) || isRMAGetFunc(std::string(targetFuncName))){
		errs() << "Simplify RMA Found " << targetFuncName <<"\n";
		BasicBlock *newBB = b->splitBasicBlock(i, "shmem_RDMA_bb");
		simplify_RMA(callInst, C, b,newBB);	
		Instruction *tmp = i;
		isSplit = true;
		tmp->eraseFromParent();
		/*		IRBuilder<> Builder(callInst);
		Value *NumBytes = callInst->getOperand(2);
		Value *LoadBasePtr = callInst->getArgOperand(1);
		Value *StoreBasePtr = callInst->getArgOperand(0);
		Value *Ops[] = {StoreBasePtr, LoadBasePtr, NumBytes};//, Builder.getInt32(std::min(SI->getAlignment(), LI->getAlignment())), Builder.getInt1(true) };
		errs() << "saa create memcpy dou1"  <<"\n";
		Module *mod = i->getParent()->getParent()->getParent();
 
		Constant *c = mod->getOrInsertFunction("shmem_put_new", IntegerType::get(C, 32), StoreBasePtr->getType(), LoadBasePtr->getType(), NumBytes->getType(), NULL);
 		CallInst *NewCall = CallInst::Create(c, Ops, "putcall");
		errs() << "saa create memcpy dou2"  <<"\n";
		Builder.GetInsertBlock()->getInstList().insert(Builder.GetInsertPoint(),NewCall);*/
	      }
	    }
	  }
	}
      }
      return true;
    }
    void getAnalysisUsage(AnalysisUsage &AU) const override;
  };
}

char SPIRERMAtoLS::ID = 0;
static RegisterPass<SPIRERMAtoLS> X("SPIRERMAtoLS", "SPIRE for SHMEM: Converting RDMA function calls to simple load store operations", false, false);


// This example modifies the program, but does not modify the CFG
void SPIRERMAtoLS::getAnalysisUsage(AnalysisUsage &AU) const {
  //AU.setPreservesCFG();
  //AU.setPreservesAll();
}
